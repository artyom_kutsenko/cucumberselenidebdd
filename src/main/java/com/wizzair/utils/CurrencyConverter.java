package com.wizzair.utils;


import com.fasterxml.jackson.databind.JsonNode;
import org.apache.http.HttpResponse;

import java.math.BigDecimal;

import static com.wizzair.config.ConfigUtil.getProperties;
import static com.wizzair.utils.RequestUtils.prepareJson;
import static com.wizzair.utils.RequestUtils.sendGet;

public class CurrencyConverter {

    public static BigDecimal PLN_USD_RATE;

    public static BigDecimal convertToUsd(String from, BigDecimal amount) {
        BigDecimal rate;
        if (from.equals("PLN")) {
            rate = getPlnToUsdRate();
        } else {
            rate = getToUsdRate(from);
        }
        return amount.divide(rate, getProperties().roundingScale(), BigDecimal.ROUND_HALF_UP);
    }

    private static BigDecimal getPlnToUsdRate() {
        if (PLN_USD_RATE == null) {
            PLN_USD_RATE = getToUsdRate("PLN");
        }
        return PLN_USD_RATE;
    }

    private static BigDecimal getToUsdRate(String from) {
        HttpResponse httpResponse = sendGet(getProperties().exchangeCurrencyUrl().replace("?code=", "?code=" + from));
        JsonNode jsonNode = prepareJson(httpResponse);
        return new BigDecimal(jsonNode.get("response").get("currency").get("rate_per_usd").asText());
    }


}
