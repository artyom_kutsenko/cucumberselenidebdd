package com.wizzair.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;


public class RequestUtils {

    public static HttpResponse sendGet(String url) {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        try {
            return client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(String.format("Got error when sent GET request to: %s", url));
        }
    }

    public static JsonNode prepareJson(HttpResponse httpResponse) {
        try {
            return new ObjectMapper().readTree(httpResponse.getEntity().getContent());
        } catch (IOException e) {
            throw new RuntimeException("Cannot parse response: " + httpResponse);
        }
    }

}
