package com.wizzair.utils;

import com.wizzair.models.Email;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

import static com.wizzair.config.ConfigUtil.getProperties;


public class EmailSender {

    public static void sendEmail(Email email) {
        try {
            Properties mailServerProperties = System.getProperties();
            mailServerProperties.put("mail.smtp.port", "587");
            mailServerProperties.put("mail.smtp.auth", "true");
            mailServerProperties.put("mail.smtp.starttls.enable", "true");

            Session getMailSession = Session.getDefaultInstance(mailServerProperties, null);
            MimeMessage generateMailMessage = new MimeMessage(getMailSession);
            generateMailMessage.addFrom(InternetAddress.parse(getProperties().senderEmail()));
            generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(getProperties().recipientEmail()));
            generateMailMessage.setSubject(email.getSubject());
            generateMailMessage.setContent(email.getBody(), "text/html");

            Transport transport = getMailSession.getTransport("smtp");
            transport.connect("smtp.gmail.com", getProperties().senderEmail(), getProperties().senderPassword());
            transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
            transport.close();
        } catch (MessagingException e) {
            System.err.println("Cannot send email, error occurred:\n");
            e.printStackTrace();
        }

    }

}
