package com.wizzair.steps;

import com.wizzair.core.BaseSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class BasicFareFinderSteps extends BaseSteps {

    @And("^I set origin \"([^\"]*)\"$")
    public void setOrigin(String origin) {
        contextHandler
                .getBasicFareFinder()
                .setOrigin(origin);
    }

    @And("^I set destination \"([^\"]*)\"$")
    public void setDestination(String destination) {
        contextHandler
                .getBasicFareFinder()
                .setDestination(destination);
    }

    @And("^I select WizzClub option$")
    public void selectWizzClubOption() {
        contextHandler
                .getBasicFareFinder()
                .selectWizzClubCheckBox(true);
    }

    @When("^I click on Search button$")
    public void clickSearchButton() {
        contextHandler
                .getBasicFareFinder()
                .clickOnSearchButton();
    }

}
