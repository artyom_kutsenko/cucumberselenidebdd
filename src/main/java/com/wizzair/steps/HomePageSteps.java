package com.wizzair.steps;

import com.wizzair.core.BaseSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;

public class HomePageSteps extends BaseSteps {

    @Given("^I am on the home page$")
    public void iAmOnTheHomePage() {
        contextHandler
                .getHomePage()
                .openPage();
    }

    @And("^I select flexible on dates option$")
    public void selectFlexibleDatesOption() {
        contextHandler
                .getHomePage()
                .selectFlexibleOnDatesOption();
    }

}
