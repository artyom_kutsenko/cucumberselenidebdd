package com.wizzair.steps;

import com.wizzair.core.BaseSteps;
import com.wizzair.models.Flight;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.List;

import static com.wizzair.models.Flight.filterFlightsByPrice;
import static com.wizzair.pages.MonthResultsPage.ResultsBlock.OUTBOUND;
import static com.wizzair.pages.MonthResultsPage.ResultsBlock.RETURN;

public class MonthResultsSteps extends BaseSteps {

    @When("^I get results for \"([^\"]*)\" months from origin city$")
    public void getResultsFromOriginCity(int monthsNumber) {
        List<Flight> flightsFromOrigin = contextHandler
                .getMonthResultsPage()
                .getFlights(monthsNumber, OUTBOUND);
        contextHandler.addCheapFlights(filterFlightsByPrice(flightsFromOrigin));
    }

    @And("^I get results for \"([^\"]*)\" months back to origin city$")
    public void getResultsBackToOriginCity(int monthsNumber) {
        List<Flight> flightsBackToOrigin = contextHandler
                .getMonthResultsPage()
                .getFlights(monthsNumber, RETURN);
        contextHandler.addCheapFlights(filterFlightsByPrice(flightsBackToOrigin));
    }

    @Then("^I am on Month results page$")
    public void checkIfCorrectPageIsOpened() {
        Assert.assertTrue(contextHandler
                .getMonthResultsPage()
                .isCorrectPageOpened());
    }
}
