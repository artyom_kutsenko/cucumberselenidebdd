package com.wizzair.steps;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

import static com.wizzair.config.ConfigUtil.getProperties;

public class Hooks implements En {

    @Before()
    public void beforeScenario() {
        String chromePath = System.getProperty("chrome.driver.path", getProperties().chromeDriverPath());
        String firefoxPath = System.getProperty("firefox.driver.path", getProperties().firefoxDriverPath());
        System.setProperty("webdriver.chrome.driver", chromePath);
        System.setProperty("webdriver.gecko.driver", firefoxPath);
        Configuration.timeout = getProperties().defaultTimeout();
        Configuration.browser = getProperties().browser();
    }

    @After("@wizzair")
    public void afterScenario() {
        Selenide.close();
    }
}
