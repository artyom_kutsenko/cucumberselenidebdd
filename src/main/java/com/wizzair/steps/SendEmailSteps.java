package com.wizzair.steps;


import com.wizzair.core.BaseSteps;
import com.wizzair.models.Email;
import cucumber.api.java.en.Then;

import java.util.stream.Collectors;

import static com.wizzair.utils.EmailSender.sendEmail;

public class SendEmailSteps extends BaseSteps {

    @Then("^I want to receive just found cheap flights by email$")
    public void sendResults() {
        if (areThereAnyCheapFlights()) {
            sendEmail(new Email(contextHandler.getCheapFlights(), "WizzAir"));
        }
    }

    protected boolean areThereAnyCheapFlights() {
        return !contextHandler.getCheapFlights().stream()
                .filter(flightGroup -> !flightGroup.isEmpty())
                .collect(Collectors.toList())
                .isEmpty();
    }
}
