package com.wizzair.core;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.wizzair.config.ConfigUtil.getProperties;

public class BasePage {

    protected WebDriverWait wait = new WebDriverWait(getWebDriver(), getProperties().defaultTimeout());

    protected void scrollToElement(SelenideElement element) {
        executeJavaScript("arguments[0].scrollIntoView(true);", element);
    }

    protected void waitTillOptionIsSelectedInDropdown(SelenideElement select, String optionText) {
        new FluentWait<>(WebDriverRunner.getWebDriver())
                .withTimeout(getProperties().defaultTimeout(), TimeUnit.MILLISECONDS)
                .pollingEvery(10, TimeUnit.MILLISECONDS)
                .until(driver -> (select.getSelectedOption().text().trim().equals(optionText)) ? true : false);
    }

}
