package com.wizzair.core;

import com.wizzair.models.Flight;
import com.wizzair.pages.BasicFareFinder;
import com.wizzair.pages.HomePage;
import com.wizzair.pages.MonthResultsPage;

import java.util.ArrayList;
import java.util.List;

// A "World" instance for sharing context between steps
public class ContextHandler {

    private HomePage homePage;
    private BasicFareFinder basicFareFinder;
    private MonthResultsPage monthResultsPage;
    private List<List<Flight>> cheapFlights = new ArrayList<>();

    public void addCheapFlights(List<Flight> flights) {
        cheapFlights.add(flights);
    }

    public List<List<Flight>> getCheapFlights() {
        return cheapFlights;
    }

    public HomePage getHomePage() {
        if (null == homePage) {
            homePage = new HomePage();
        }
        return homePage;
    }

    public BasicFareFinder getBasicFareFinder() {
        if (null == basicFareFinder) {
            basicFareFinder = new BasicFareFinder();
        }
        return basicFareFinder;
    }

    public MonthResultsPage getMonthResultsPage() {
        if (null == monthResultsPage) {
            monthResultsPage = new MonthResultsPage();
        }
        return monthResultsPage;
    }
}
