package com.wizzair.config;


import org.aeonbits.owner.Config;

import java.math.BigDecimal;

@Config.HotReload
@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "file:${user.dir}/src/main/java/com/wizzair/config/properties/common.properties",
})
public interface CoreConfig extends Config {

    @Key("default.timeout")
    int defaultTimeout();

    @Key("wizzair.base.url")
    String wizzairBaseURL();

    @Key("ryanair.base.url")
    String ryanairBaseURL();

    @Key("origin")
    String origin();

    @Key("months.number")
    String monthsNumber();

    @Key("chrome.driver.path")
    String chromeDriverPath();

    @Key("firefox.driver.path")
    String firefoxDriverPath();

    @Key("max.usd.price")
    BigDecimal maxUsdPrice();

    @Key("exchange.currency.url")
    String exchangeCurrencyUrl();

    @Key("rounding.scale")
    Integer roundingScale();

    @Key("sender.email")
    String senderEmail();

    @Key("sender.password")
    String senderPassword();

    @Key("recipient.email")
    String recipientEmail();

    @Key("browser")
    String browser();

    @Key("hub.url")
    String hubUrl();
}
