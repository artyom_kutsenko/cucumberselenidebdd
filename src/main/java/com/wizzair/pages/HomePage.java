package com.wizzair.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import com.wizzair.core.BasePage;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;
import static com.wizzair.config.ConfigUtil.getProperties;

public class HomePage extends BasePage {

    private static final String MATCHED_CITY_LOCATOR = "//div[contains(@class,'flight-search__panel__locations-container')]//strong[@class='locations-container__location__name']";
    private SelenideElement flexibleOnDatesOption = $(byXpath("//form[@name='flight-search']//button[contains(.,'Flexible')]"));
    private SelenideElement originField = $(byXpath("//input[@data-test='search-departure-station']"));
    private SelenideElement matchedCity = $(byXpath(MATCHED_CITY_LOCATOR));
    private ElementsCollection matchedCities = $$(byXpath(MATCHED_CITY_LOCATOR));
    private SelenideElement destination = $(byXpath("//input[@data-test='search-arrival-station']"));

    public HomePage openPage() {
        open(getProperties().wizzairBaseURL());
        return this;
    }

    public BasicFareFinder selectFlexibleOnDatesOption() {
        flexibleOnDatesOption.click();
        return new BasicFareFinder();
    }

    public HomePage selectOrigin(String origin) {
        originField.clear();
        originField.setValue(origin);
        matchedCity.click();
        return this;
    }

    public List<String> getAvailableDestinations() {
        destination.click();
        return matchedCities.stream()
                .map(city -> {
                    scrollToElement(city);
                    return city.text();
                })
                .collect(Collectors.toList());
    }

}
