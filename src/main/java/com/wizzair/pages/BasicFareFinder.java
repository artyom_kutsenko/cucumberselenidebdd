package com.wizzair.pages;

import com.codeborne.selenide.SelenideElement;
import com.wizzair.core.BasePage;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class BasicFareFinder extends BasePage {

    private SelenideElement origin = $(byXpath("//input[@data-test='search-departure-station']"));
    private SelenideElement destination = $(byXpath("//input[@data-test='search-arrival-station']"));
    private SelenideElement wizzClubCheckBox = $(byXpath("//label[@for='fare-finder-wdc' and @class='rf-checkbox__label']"));
    private SelenideElement searchButton = $(byXpath("//div[@class='fare-finder__sidebar__submit']/*[contains(.,'Search') or contains(.,'Submit')or contains(.,'SUBMIT')]"));
    private SelenideElement matchedCity = $(byXpath("//div[contains(@class,'flight-search__panel__locations-container')]//strong[@class='locations-container__location__name']"));

    private String uncheckedCheckBoxBorderBottomColor = "rgba(145, 145, 145, 1)";
    private String checkedIndicatorStyle = "border-bottom-color";

    public BasicFareFinder setOrigin(String origin) {
        this.origin.clear();
        this.origin.setValue(origin);
        matchedCity.click();
        return this;
    }

    public BasicFareFinder setDestination(String destination) {
        this.destination.clear();
        this.destination.setValue(destination);
        matchedCity.click();
        return this;
    }

    public BasicFareFinder selectWizzClubCheckBox(boolean shouldBeSet) {
        if (shouldBeSet && !isCheckBoxSelected()) {
            wizzClubCheckBox.click();
        } else if (!shouldBeSet && isCheckBoxSelected()) {
            wizzClubCheckBox.click();
        }
        return this;
    }

    public boolean isCheckBoxSelected() {
        return !wizzClubCheckBox.getCssValue(checkedIndicatorStyle).equals(uncheckedCheckBoxBorderBottomColor);
    }

    public MonthResultsPage clickOnSearchButton() {
        searchButton.click();
        return new MonthResultsPage();
    }

}
