package com.wizzair.pages;


import com.codeborne.selenide.SelenideElement;
import com.wizzair.core.BasePage;
import com.wizzair.models.Flight;
import com.wizzair.models.FlightBuilder;

import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.wizzair.config.ConfigUtil.getProperties;

public class MonthResultsPage extends BasePage {

    private static final String OUTBOUND_BLOCK = "//div[contains(@class,'outbound')]";
    private static final String RETURN_BLOCK = "//div[contains(@class,'return')]";
    private static final String PERIOD_DROP_DOWN = "//select[contains(@class,'dropdown')]";
    private static final String CITIES = "//address[contains(@class,'box__title')]";
    private static final String CALENDAR_HEADER = "//header[@class='fare-finder__calendar__header']";
    private static final String PRICE_IN_CONTAINER = "//p/span[contains(@class,'price-price')]";
    private static final String CURRENCY_CODE_IN_CONTAINER = "//p/span[contains(@class,'price-currency')]";
    private static final String DAY_IN_CONTAINER = "//i[contains(@class, 'highlight-with-selection')]";
    private static final String TIME_IN_CONTAINER = "//p[contains(@class,'time')]";
    private static final String DAY_CONTAINERS = "//i[contains(@class,'highlight-with-selection')]/..";
    private static final String SHOW_DEPARTURE_TIMES = "//label[@for='timetable-toggle']";
    private SelenideElement loadingWheel = $(byXpath("//div[@class='loader-combined snow-basic']"));
    private DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("MMMM yyyy");

    public MonthResultsPage selectShowDepartureTimesOption(ResultsBlock resultsBlock) {
        SelenideElement element = $(byXpath(resultsBlock.getBlock() + SHOW_DEPARTURE_TIMES));
        if (element.exists() && !isShowDepartureTimeOptionSelected(resultsBlock.getBlock() + SHOW_DEPARTURE_TIMES)) {
            element.click();
        }
        return this;
    }

    private boolean isShowDepartureTimeOptionSelected(String elementXpath) {
        String selected = $(byXpath(elementXpath + "/../input[@type='checkbox']")).getAttribute("checked");
        return selected != null && selected.equals("true");
    }

    public MonthResultsPage selectPeriod(YearMonth yearMonth, ResultsBlock resultsBlock) {
        String optionToSelect = yearMonth.format(monthYearFormatter);
        if (!$(byXpath(resultsBlock.getBlock() + PERIOD_DROP_DOWN)).getSelectedOption().text().trim().equals(optionToSelect)) {
            $(byXpath(resultsBlock.getBlock() + PERIOD_DROP_DOWN)).selectOption(optionToSelect);
            loadingWheel.waitUntil(disappear, getProperties().defaultTimeout());
            waitTillOptionIsSelectedInDropdown($(byXpath(resultsBlock.getBlock() + PERIOD_DROP_DOWN)), optionToSelect);
            $(byXpath(resultsBlock.getBlock())).shouldBe(enabled);
        }
        return this;
    }

    public List<Flight> getFlights(int numberOfMonths, ResultsBlock resultsBlock) {
        $(byXpath(CALENDAR_HEADER)).should(appear);
        List<Flight> flights = new ArrayList<>();
        YearMonth month = YearMonth.now().minusMonths(1);
        int iterations = 0;
        SelenideElement cities = $(byXpath(resultsBlock.getBlock() + CITIES));
        scrollToElement(cities);
        String origin = cities.text().split("\\) ")[0].trim() + ")";
        String destination = cities.text().split("\\) ")[1].trim();
        do {
            month = month.plusMonths(1);
            selectPeriod(month, resultsBlock)
                    .selectShowDepartureTimesOption(resultsBlock);

            int resultsNumber = $$(byXpath(resultsBlock.getBlock() + DAY_CONTAINERS)).size();
            for (int i = 1; i <= resultsNumber; i++) {
                String containerXpath = String.format("(%s)[%d]", resultsBlock.getBlock() + DAY_CONTAINERS, i);
                String price = $(byXpath(containerXpath + PRICE_IN_CONTAINER)).text();
                String ccyCode = $(byXpath(containerXpath + CURRENCY_CODE_IN_CONTAINER)).text();
                String day = $(byXpath(containerXpath + DAY_IN_CONTAINER)).text();
                String time = $(byXpath(containerXpath + TIME_IN_CONTAINER)).text();
                LocalDateTime dateTime = LocalDateTime.of(month.getYear(), month.getMonth(), Integer.parseInt(day),
                        Integer.parseInt(time.split(":")[0]), Integer.parseInt(time.split(":")[1]));
                Flight flight = new FlightBuilder()
                        .withOrigin(origin)
                        .withDestination(destination)
                        .withDate(dateTime)
                        .withCurrencyCode(ccyCode)
                        .withPrice(price)
                        .build();
                flights.add(flight);
            }
            iterations += 1;
        } while (numberOfMonths > iterations);

        return flights;
    }

    public boolean isCorrectPageOpened() {
        $(byXpath(CALENDAR_HEADER)).waitUntil(visible, getProperties().defaultTimeout());
        return $$(byXpath(CALENDAR_HEADER)).size() > 1;
    }

    public enum ResultsBlock {

        OUTBOUND(OUTBOUND_BLOCK), RETURN(RETURN_BLOCK);

        private String block;

        ResultsBlock(String block) {
            this.block = block;
        }

        public String getBlock() {
            return block;
        }
    }

}
