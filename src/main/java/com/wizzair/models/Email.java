package com.wizzair.models;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class Email {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd MMM yyyy E hh:mm");
    private static final String RESULTS_MESSAGE_SUBJECT = "Cheap flights from %s " + LocalDate.now();
    private static final String SCRIPT_ISSUE_SUBJECT = "%s script is broken " + LocalDate.now();
    private static final String SCRIPT_ISSUE_BODY = "Last execution was failed, check the validity of the script";
    private static final String EMAIL_BODY_START =
            "<html>\n" +
                    "<head>" +
                    "<style>\n" +
                    "table {\n" +
                    "    font-family: arial, sans-serif;\n" +
                    "    border-collapse: collapse;\n" +
                    "    width: 100%;\n" +
                    "}\n" +
                    "\n" +
                    "td, th {\n" +
                    "    border: 1px solid #dddddd;\n" +
                    "    text-align: left;\n" +
                    "    padding: 8px;\n" +
                    "}\n" +
                    "\n" +
                    "tr:nth-child(even) {\n" +
                    "    background-color: #dddddd;\n" +
                    "}\n" +
                    "</style>\n" +
                    "</head>\n" +
                    "<body>";
    private static final String TABLE_START =
            "<table>\n" +
                    "  <tr>\n" +
                    "    <th>Origin</th>\n" +
                    "    <th>Destination</th>\n" +
                    "    <th>Departure Date&Time</th>\n" +
                    "    <th>Price in local CCY</th>\n" +
                    "    <th>Price in USD</th>\n" +
                    "  </tr>";
    private static final String TABLE_HEADER = "<thead>\n" +
            "<tr>\n" +
            "<th colspan = \"5\" bgcolor = \"#E6E6FA\">%s</th>\n" +
            "</tr>\n" +
            "</thead>";
    private static final String ROW_TEMPLATE = "<tr>\n" +
            "<td>%s</td>\n" +
            "<td>%s</td>\n" +
            "<td>%s</td>\n" +
            "<td>%s</td>\n" +
            "<td>%s</td>\n" +
            "</tr>";
    private static final String TABLE_END = "</table>";
    private static final String EMAIL_BODY_END =  "</body>\n </html>";

    private String body;
    private String subject;


    public Email(List<List<Flight>> cheapFlights, String airCompany) {
        body = getResultsEmailBody(cheapFlights);
        subject = String.format(RESULTS_MESSAGE_SUBJECT, airCompany);
    }

    public Email(String testMethod) {
        this.body = SCRIPT_ISSUE_BODY;
        this.subject = String.format(SCRIPT_ISSUE_SUBJECT, testMethod);
    }
    public String getBody() {
        return body;
    }

    public String getSubject() {
        return subject;
    }

    private String getResultsEmailBody(List<List<Flight>> flights) {
        StringBuilder emailBody = new StringBuilder();
        emailBody.append(EMAIL_BODY_START);
        flights.stream().filter(group -> !group.isEmpty()).forEach(group -> {
            String tableContent = group.stream().map(flight ->
                    String.format(ROW_TEMPLATE, flight.getOrigin(), flight.getDestination(),
                            flight.getDateTime().format(FORMATTER),
                            flight.getPriceInInitialCurrency() + " " + flight.getCurrencyCode(),
                            flight.getPriceInUsd() + " USD"))
                    .collect(Collectors.joining(" \n"));

            emailBody.append(TABLE_START)
                    .append(String.format(TABLE_HEADER, group.get(0).getOrigin() + " -> " + group.get(0).getDestination()))
                    .append(tableContent)
                    .append(TABLE_END).append("<br><br>");
        });
        return emailBody.append(EMAIL_BODY_END).toString();
    }

}
