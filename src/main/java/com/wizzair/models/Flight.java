package com.wizzair.models;


import com.google.common.base.Objects;
import com.wizzair.utils.CurrencyConverter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.wizzair.config.ConfigUtil.getProperties;

public class Flight {

    private LocalDateTime date;
    private String currencyCode;
    private BigDecimal price;
    private BigDecimal priceInUsd;
    private String origin;
    private String destination;

    private static final BigDecimal MAX_PRICE_USD = getProperties().maxUsdPrice();

    public Flight(FlightBuilder flightBuilder) {
        origin = flightBuilder.getOrigin();
        destination = flightBuilder.getDestination();
        date = flightBuilder.getDate();
        currencyCode = flightBuilder.getCurrencyCode();
        price = new BigDecimal(flightBuilder.getPrice().replaceAll("[^.\\d]", ""));
        priceInUsd = (currencyCode.equals("USD"))
                ? this.price
                : CurrencyConverter.convertToUsd(currencyCode, this.price);
    }

    public LocalDateTime getDateTime() {
        return date;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public BigDecimal getPriceInInitialCurrency() {
        return price;
    }

    public BigDecimal getPriceInUsd() {
        return priceInUsd;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public static List<Flight> filterFlightsByPrice(List<Flight> allFlights) {
        return allFlights.stream()
                .filter(flight -> flight.getPriceInUsd().compareTo(MAX_PRICE_USD) <= 0)
                .collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return Objects.equal(getDateTime(), flight.getDateTime()) &&
                Objects.equal(getCurrencyCode(), flight.getCurrencyCode()) &&
                Objects.equal(price, flight.price) &&
                Objects.equal(getPriceInUsd(), flight.getPriceInUsd()) &&
                Objects.equal(getOrigin(), flight.getOrigin()) &&
                Objects.equal(getDestination(), flight.getDestination());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getDateTime(), getCurrencyCode(), price, getPriceInUsd(), getOrigin(), getDestination());
    }

}
