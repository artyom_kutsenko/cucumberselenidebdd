package com.wizzair.models;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class FlightBuilder {

    private LocalDateTime date;
    private String currencyCode;
    private String price;
    private String origin;
    private String destination;

    public LocalDateTime getDate() {
        return date;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getPrice() {
        return price;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public FlightBuilder withDate(LocalDateTime date) {
        this.date = date;
        return this;
    }

    public FlightBuilder withCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public FlightBuilder withPrice(String price) {
        this.price = price;
        return this;
    }

    public FlightBuilder withOrigin(String origin) {
        this.origin = origin;
        return this;
    }

    public FlightBuilder withDestination(String destination) {
        this.destination = destination;
        return this;
    }

    public Flight build() {
        return new Flight(this);
    }


}
