Feature: Cheap Flights Search

As a Wizzair user
In order to buy the cheapest tickets ever
I want to fetch cheap flights from Wizzair website and receive them by email

@wizzair
Scenario Outline: Get cheap flights from provided Destination based on filter criteria

  Given I am on the home page
    And I select flexible on dates option
    And I set origin "<Origin>"
    And I set destination "<Destination>"
    And I select WizzClub option
  When I click on Search button
  Then I am on Month results page
  When I get results for "<Months>" months from origin city
    And I get results for "<Months>" months back to origin city
  Then I want to receive just found cheap flights by email

Examples:
| Origin  | Destination  | Months |
| Wroclaw | London Luton | 3      |
| Wroclaw | Birmingham   | 3      |